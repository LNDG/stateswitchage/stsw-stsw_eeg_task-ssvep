# StateSwitch 30 Hz SSVEP

## Steady-state visual-evoked potential

The SSVEP characterizes the phase-locked, entrained visual activity (here 30 Hz) during dynamic stimulus updates (e.g., ref. 89). These features differentiate it from induced broadband activity or muscle artifacts in similar frequency bands. We used these properties to normalize individual single-trial SSVEP responses prior to averaging: (a) we calculated an FFT for overlapping 1 s segments with a step size of 100 ms (Hanning-based multitaper) and averaged them within each load condition; (b) we spectrally normalized 30 Hz estimates by subtracting the average of estimates at 28 and 32 Hz, effectively removing broadband effects (i.e., aperiodic slopes), and; (c) we subtracted a temporal baseline −700 to −100 ms prior to stimulus onset. Linear load effects on SSVEPs were assessed by univariate cluster-based permutation tests on channel   time data (see “Univariate cluster-based permutation analyses”).

Single-trial discrete FFT transform, data are then averaged and are additionally be spectrally normalized by subtracting the two adjacent frequencies, removing more broadband power (cf. spectrally specific SSVEP). This seems to work best when using sliding windows and single-trial transforms, presumably because muscle noise is non-stationary. SSVEPs are clearly influenced by SNR or averaging, with clearer representations upon averaging. Care has to be taken to avoid spectral leakage. That is, the time segmentation has to be chosen to be multiples of the target frequency (30 Hz here).

Alternatives that also work are to (a) apply spectral normalization at the single trial level (may be interesting to investigate trial-by-trial e.g., alpha-SSVEP coupling) or to (b) spectrally transform the across-trial ERP (as background fluctuations - albeit not systematic changes - should be removed).

**a_calculate_SSVEP**

- Here, we extract the 30 Hz SSVEP using intervals covering 15 cycles with a step size of 250 ms. Then we offer multiple outputs regarding normalization: spectral correction means that the average of the two adjacent frequency bands is subtracted from the 30 Hz estimate, effectively controlling for inter-individual differences in spectral slope and more broad-band noise around the 30 Hz band.
- Note: Normalized variants are currently unused, normalization is applied in subsequent steps

**b_plot_SSVEP**

- apply normalization to across-trial average
- apply baseline correction (mean from-700 to -100 ms prestim)

![figure](figures/b_toposNormalizedvsOriginalSSVEPYAOA.png)

**c_SSVEP_CBPA**

- perform CBPA against baseline and for parameteric load effect
- call separately for YA, OA and merged groups

**d_overview_SSVEP**

- plot topographies and time traces for original and normalized data (both bl-corrected)

Normalized:
![figure](figures/d_SSVEPnormalizedOverview_YAOA.png)

Original:
![figure](figures/d_SSVEPoriginalOverview_YAOA.png)

**e_compare_SSVEP_byAge_normalized**

- visually compare YA and OA traces (spectrally normalized and baselined data)
![figure](figures/e_SSVEP_byAge.png)