function b_plot_SSVEP(group)

if ismac % run if function is not pre-compiled
    currentFile = mfilename('fullpath');
    [pathstr,~,~] = fileparts(currentFile);
    cd(fullfile(pathstr,'..'))
    rootpath = pwd;
end

if strcmp(group, 'YA')
    % N = 47;
    IDs = {'1117';'1118';'1120';'1124';'1126';'1131';'1132';'1135';'1136';'1138';...
        '1144';'1151';'1158';'1160';'1163';'1164';'1167';'1169';'1172';'1173';...
        '1178';'1182';'1215';'1216';'1219';'1221';'1223';'1227';'1228';'1233';...
        '1234';'1237';'1239';'1240';'1243';'1245';'1247';'1250';'1252';'1257';...
        '1261';'1265';'1266';'1268';'1270';'1276';'1281'};
elseif strcmp(group, 'OA')
    %N = 53 OAs
    IDs = {'2104';'2107';'2108';'2112';'2118';'2120';'2121';'2123';'2125';'2129';...
        '2130';'2131';'2132';'2133';'2134';'2135';'2139';'2140';'2145';'2147';...
        '2149';'2157';'2160';'2201';'2202';'2203';'2205';'2206';'2209';'2210';...
        '2211';'2213';'2214';'2215';'2216';'2217';'2219';'2222';'2224';'2226';...
        '2227';'2236';'2237';'2238';'2241';'2244';'2246';'2248';'2250';'2251';...
        '2252';'2258';'2261'};
elseif strcmp(group, 'YAOA')
    filename = fullfile(rootpath, 'code', 'id_list.txt');
    fileID = fopen(filename);
    IDs = textscan(fileID,'%s');
    fclose(fileID);
    IDs = IDs{1};
end

pn.dataIn	= fullfile(rootpath, '..', 'X1_preprocEEGData');
pn.out      = fullfile(rootpath, 'data', 'individualFFT');
pn.tools	= fullfile(rootpath, 'tools'); addpath(pn.tools);
pn.figures  = fullfile(rootpath, 'figures');
addpath(fullfile(pn.tools, 'fieldtrip')); ft_defaults;
addpath(fullfile(pn.tools, 'BrewerMap'));

OriginalSSVEP = [];
for indID = 1:numel(IDs)
    tmp = load(fullfile(pn.out, [IDs{indID},'_SSVEP.mat']));
    for indCond = 1:4
        OriginalSSVEP(indID,indCond,:,:,:) = tmp.MTMdata{indCond}.powspctrm_avg;
    end
    time = tmp.MTMdata{indCond}.time;
    freq = tmp.MTMdata{indCond}.freq;
end

% spectral correction
for indFreq = 2:30-1
   NormalizedSSVEP(:,:,:,:,indFreq) = (OriginalSSVEP(:,:,:,:,indFreq)-...
    nanmean(OriginalSSVEP(:,:,:,:,[indFreq-1,indFreq+1]),5))./...
    nanmean(OriginalSSVEP(:,:,:,:,[indFreq-1,indFreq+1]),5);
end

blTimeIdx = find(tmp.MTMdata{indCond}.time > 2.3 & tmp.MTMdata{indCond}.time <2.9);

originalBaseline = repmat(nanmean(nanmean(OriginalSSVEP(:,:,:,blTimeIdx,:),4),2),1,4,1,66,1);
normalizedBaseline = repmat(nanmean(nanmean(NormalizedSSVEP(:,:,:,blTimeIdx,:),4),2),1,4,1,66,1);

OriginalSSVEP_bl = (OriginalSSVEP-originalBaseline)./originalBaseline;
NormalizedSSVEP_bl = (NormalizedSSVEP-normalizedBaseline);

%% save SSVEP as output

SSVEPmag.norm.data = squeeze(nanmean(nanmean(NormalizedSSVEP_bl(:,1:4,58:60,time>3& time<6, 15),3),4));
SSVEPmag.orig.data = squeeze(nanmean(nanmean(OriginalSSVEP_bl(:,1:4,58:60,time>3& time<6, 15),3),4));
SSVEPmag.IDs = IDs;

save(fullfile(pn.out, ['Z_SSVEPmag_',group,'.mat']), 'SSVEPmag')

%% plot time-frequency representation of different variants

h = figure('units','normalized','position',[.1 .1 .6 .5]);
subplot(2,2,1); imagesc(time,freq,squeeze(nanmean(nanmean(OriginalSSVEP(:,1,58:60,:, :),3),1))'); xlabel('Time (s)'); ylabel('Freq (Hz)'); title('Original, non-normalized')
subplot(2,2,2); imagesc(time,freq,squeeze(nanmean(nanmean(OriginalSSVEP_bl(:,1,58:60,:, :),3),1))'); xlabel('Time (s)'); ylabel('Freq (Hz)'); title('Original, temp. normalized')
subplot(2,2,3); imagesc(time,freq,squeeze(nanmean(nanmean(NormalizedSSVEP(:,1,58:60,:, :),3),1))'); xlabel('Time (s)'); ylabel('Freq (Hz)'); title('spectrally normalized')
subplot(2,2,4); imagesc(time,freq,squeeze(nanmean(nanmean(NormalizedSSVEP_bl(:,1,58:60,:, :),3),1))'); xlabel('Time (s)'); ylabel('Freq (Hz)'); title('spectrally, temp. normalized')
set(findall(gcf,'-property','FontSize'),'FontSize',18)

figureName = ['b_TFR_OriginalNormalized_', group];
saveas(h, fullfile(pn.figures, figureName), 'epsc');
saveas(h, fullfile(pn.figures, figureName), 'png');

%% plot original and spectrally normalized spectra

h = figure('units','normalized','position',[.1 .1 .4 .2]);
subplot(1,2,1); hold on;
    plot(log10(freq), squeeze(nanmedian(nanmedian(nanmedian(log10(OriginalSSVEP(:,1,58:60,time>3 & time<6, :)),3),4),1)), 'LineWidth', 2)
    plot(log10(freq), squeeze(nanmedian(nanmedian(nanmedian(log10(OriginalSSVEP(:,2,58:60,time>3 & time<6, :)),3),4),1)), 'LineWidth', 2)
    plot(log10(freq), squeeze(nanmedian(nanmedian(nanmedian(log10(OriginalSSVEP(:,3,58:60,time>3 & time<6, :)),3),4),1)), 'LineWidth', 2)
    plot(log10(freq), squeeze(nanmedian(nanmedian(nanmedian(log10(OriginalSSVEP(:,4,58:60,time>3 & time<6, :)),3),4),1)), 'LineWidth', 2)
    xlabel('Frequency (Hz)'); ylabel('Power (log10)');
    title('Non-normalized estimate');
    legend({'Load 1'; 'Load 2'; 'Load 3'; 'Load 4'}); legend('boxoff')
subplot(1,2,2); hold on;
    plot(freq, [squeeze(nanmedian(nanmedian(nanmedian(NormalizedSSVEP(:,1,58:60,time>3 & time<6, :),3),4),1));0], 'LineWidth', 2)
    plot(freq, [squeeze(nanmedian(nanmedian(nanmedian(NormalizedSSVEP(:,2,58:60,time>3 & time<6, :),3),4),1));0], 'LineWidth', 2)
    plot(freq, [squeeze(nanmedian(nanmedian(nanmedian(NormalizedSSVEP(:,3,58:60,time>3 & time<6, :),3),4),1));0], 'LineWidth', 2)
    plot(freq, [squeeze(nanmedian(nanmedian(nanmedian(NormalizedSSVEP(:,4,58:60,time>3 & time<6, :),3),4),1));0], 'LineWidth', 2)
    xlabel('Frequency (Hz)'); ylabel('Power (norm.)');
    title('Spectrally-normalized estimates')
set(findall(gcf,'-property','FontSize'),'FontSize',18)

figureName = ['b_SpectraNormalized_', group];
saveas(h, fullfile(pn.figures, figureName), 'epsc');
saveas(h, fullfile(pn.figures, figureName), 'png');

%% assess reliability of original and spectrally normalized measure

h = figure('units','normalized','position',[.1 .1 .4 .3]);
subplot(1,2,1);
    a = nanmean(nanmean(nanmean(OriginalSSVEP_bl(:,1:4,58:60,time>3 & time<6, 15),4),3),2);
    b = nanmean(nanmean(nanmean(NormalizedSSVEP_bl(:,1:4,58:60,time>3 & time<6, 15),4),3),2);
    scatter(a,b, 'filled'); lsline();
    [r, p] = corrcoef(a,b)
    title('BL: Spectrally normalized - original baselined SSVEP')
    xlabel('Relative SSVEP power orig. (a.u.)'); ylabel('Relative SSVEP power norm. (a.u.)')
    legend(['r = ', num2str(round(r(2),2)), ' p = ' num2str(round(p(2),3))]); legend('boxoff');
    set(findall(gcf,'-property','FontSize'),'FontSize',18)
subplot(1,2,2);
    a = nanmean(nanmean(nanmean(OriginalSSVEP_bl(:,4,58:60,time>3 & time<6, 15)-OriginalSSVEP_bl(:,1,58:60,time>3 & time<6, 15),4),3),2);
    b = nanmean(nanmean(nanmean(NormalizedSSVEP_bl(:,4,58:60,time>3 & time<6, 15)-NormalizedSSVEP_bl(:,1,58:60,time>3 & time<6, 15),4),3),2);
    scatter(a,b, 'filled'); lsline();
    [r, p] = corrcoef(a,b)
    title('Spectrally normalized - original baselined SSVEP')
    xlabel('ChangeChange: Relative SSVEP power orig. (a.u.)'); ylabel('Relative SSVEP power norm. (a.u.)')
    legend(['r = ', num2str(round(r(2),2)), ' p = ' num2str(round(p(2),3))]); legend('boxoff');
    set(findall(gcf,'-property','FontSize'),'FontSize',18)

figureName = ['J_v5_reliabilityNormalizedvsOriginalSSVEP_',group];
saveas(h, fullfile(pn.figures, figureName), 'epsc');
saveas(h, fullfile(pn.figures, figureName), 'png');

%% plot spectrally-normalized alpha and SSVEP

time = tmp.MTMdata{indCond}.time;

h = figure('units','normalized','position',[.1 .1 .7 .7]);
subplot(2,3,1); imagesc(time,[],squeeze(nanmean(NormalizedSSVEP_bl(:,4,58:60,:, 14),3)), [-2 2]); 
    title('Spectrally-norm. 28 Hz power'); xlabel('Time (samples)'); ylabel('Subjects');
subplot(2,3,2); imagesc(time,[],squeeze(nanmean(NormalizedSSVEP_bl(:,4,58:60,:, 15),3)), [-2 2]); 
    title('Spectrally-norm. SSVEP (30 Hz) power'); xlabel('Time (samples)'); ylabel('Subjects');
subplot(2,3,3); imagesc(time,[],squeeze(nanmean(NormalizedSSVEP_bl(:,4,58:60,:, 16),3)), [-2 2]); 
    title('Spectrally-norm. 32 Hz power'); xlabel('Time (samples)'); ylabel('Subjects');
subplot(2,3,4); imagesc(time,[],squeeze(nanmean(OriginalSSVEP_bl(:,4,58:60,:, 14),3)), [-2 2]); 
    title('Orig. bl. 28 Hz power'); xlabel('Time (samples)'); ylabel('Subjects');
subplot(2,3,5); imagesc(time,[],squeeze(nanmean(OriginalSSVEP_bl(:,4,58:60,:, 15),3)), [-2 2]); 
    title('Orig. bl. SSVEP (30 Hz) power'); xlabel('Time (samples)'); ylabel('Subjects');
subplot(2,3,6); imagesc(time,[],squeeze(nanmean(OriginalSSVEP_bl(:,4,58:60,:, 16),3)), [-2 2]); 
    title('Orig. bl. 32 Hz power'); xlabel('Time (samples)'); ylabel('Subjects');
set(findall(gcf,'-property','FontSize'),'FontSize',18)

figureName = ['b_contrastNormalizedvsOriginalSSVEP', group];
saveas(h, fullfile(pn.figures, figureName), 'epsc');
saveas(h, fullfile(pn.figures, figureName), 'png');

%% topoplot

cBrew = brewermap(500,'RdBu');
cBrew = flipud(cBrew);

cfg = [];
cfg.layout = 'acticap-64ch-standard2.mat';
cfg.parameter = 'powspctrm';
cfg.comment = 'no';
cfg.colorbar = 'SouthOutside';
cfg.colormap = cBrew;

h = figure('units','normalized','position',[.1 .1 .2 .2]);
subplot(1,2,1);
    plotData = [];
    plotData.label = tmp.MTMdata{indCond}.label; % {1 x N}
    plotData.dimord = 'chan';
    cfg.zlim = [-.1 .1];
    cfg.figure = h;
    plotData.powspctrm = squeeze(nanmean(nanmean(nanmean(OriginalSSVEP_bl(:,1:4,:,time>3 & time<6, 15),4),2),1));
    ft_topoplotER(cfg,plotData);
    title({'30 Hz SSVEP';'without spectral norm.'})
subplot(1,2,2);
    %cfg.zlim = [-2.5*10^-10 2.5*10^-10];
    cfg.zlim = [-.3 .3];
    cfg.figure = h;
    plotData.powspctrm = squeeze(nanmean(nanmean(nanmean(NormalizedSSVEP(:,1:4,:,time>3 & time<6, 15),4),2),1));
    ft_topoplotER(cfg,plotData);
    title({'30 Hz SSVEP';'with spectral norm.'})
set(findall(gcf,'-property','FontSize'),'FontSize',18)

figureName = ['b_toposNormalizedvsOriginalSSVEP', group];
saveas(h, fullfile(pn.figures, figureName), 'epsc');
saveas(h, fullfile(pn.figures, figureName), 'png');
