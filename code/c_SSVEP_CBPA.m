function c_SSVEP_CBPA(group)

if ismac % run if function is not pre-compiled
    currentFile = mfilename('fullpath');
    [pathstr,~,~] = fileparts(currentFile);
    cd(fullfile(pathstr,'..'))
    rootpath = pwd;
end

if strcmp(group, 'YA')
    IDs = {'1117';'1118';'1120';'1124';'1126';'1131';'1132';'1135';'1136';'1138';...
        '1144';'1151';'1158';'1160';'1163';'1164';'1167';'1169';'1172';'1173';...
        '1178';'1182';'1215';'1216';'1219';'1221';'1223';'1227';'1228';'1233';...
        '1234';'1237';'1239';'1240';'1243';'1245';'1247';'1250';'1252';'1257';...
        '1261';'1265';'1266';'1268';'1270';'1276';'1281'};
elseif strcmp(group, 'OA')
    IDs = {'2104';'2107';'2108';'2112';'2118';'2120';'2121';'2123';'2125';'2129';...
        '2130';'2131';'2132';'2133';'2134';'2135';'2139';'2140';'2145';'2147';...
        '2149';'2157';'2160';'2201';'2202';'2203';'2205';'2206';'2209';'2210';...
        '2211';'2213';'2214';'2215';'2216';'2217';'2219';'2222';'2224';'2226';...
        '2227';'2236';'2237';'2238';'2241';'2244';'2246';'2248';'2250';'2251';...
        '2252';'2258';'2261'};
elseif strcmp(group, 'YAOA')
    filename = fullfile(rootpath, 'code', 'id_list.txt');
    fileID = fopen(filename);
    IDs = textscan(fileID,'%s');
    fclose(fileID);
    IDs = IDs{1};
end

pn.dataIn	= fullfile(rootpath, '..', 'X1_preprocEEGData');
pn.out      = fullfile(rootpath, 'data', 'individualFFT');
pn.tools	= fullfile(rootpath, 'tools'); addpath(pn.tools);
pn.figures  = fullfile(rootpath, 'figures');
addpath(fullfile(pn.tools, 'fieldtrip')); ft_defaults;
addpath(fullfile(pn.tools, 'BrewerMap'));

OriginalSSVEP = [];
for indID = 1:numel(IDs)
    tmp = load(fullfile(pn.out, [IDs{indID},'_SSVEP.mat']));
    for indCond = 1:4
        OriginalSSVEP(indID,indCond,:,:,:) = tmp.MTMdata{indCond}.powspctrm_avg;
    end
    time = tmp.MTMdata{indCond}.time;
    freq = tmp.MTMdata{indCond}.freq;
    elec = tmp.MTMdata{indCond}.elec;
end

blTime = [2.3, 2.9];

% spectral correction
for indFreq = 2:30-1
   NormalizedSSVEP(:,:,:,:,indFreq) = (OriginalSSVEP(:,:,:,:,indFreq)-nanmean(OriginalSSVEP(:,:,:,:,[indFreq-1,indFreq+1]),5))./nanmean(OriginalSSVEP(:,:,:,:,[indFreq-1,indFreq+1]),5);
end

blTimeIdx = find(tmp.MTMdata{indCond}.time > blTime(1) & tmp.MTMdata{indCond}.time <blTime(2));

originalBaseline = repmat(nanmean(nanmean(OriginalSSVEP(:,:,:,blTimeIdx,:),4),2),1,4,1,66,1);
normalizedBaseline = repmat(nanmean(nanmean(NormalizedSSVEP(:,:,:,blTimeIdx,:),4),2),1,4,1,66,1);

OriginalSSVEP_bl = (OriginalSSVEP-originalBaseline)./originalBaseline;
NormalizedSSVEP_bl = (NormalizedSSVEP-normalizedBaseline);

%% CBPA: Normalized vs. baseline

CBPAstruct = []; CBPAstructZero = [];
for indCond = 1:4
    CBPAstruct{indCond,1}.powspctrmdimord = 'rpt_chan_freq_time';
    CBPAstruct{indCond,1}.time = time;
    CBPAstruct{indCond,1}.freq = freq(1:end-1);
    CBPAstruct{indCond,1}.label = elec.label;
    CBPAstruct{indCond,1}.powspctrm = squeeze(permute(NormalizedSSVEP_bl(1:47,indCond,:,:,:),[1,2,3,5,4]));
    CBPAstructZero{indCond,1} = CBPAstruct{indCond,1};
    CBPAstructZero{indCond,1}.powspctrm = squeeze(repmat(nanmean(...
        CBPAstructZero{indCond,1}.powspctrm(:,:,:,...
        CBPAstructZero{indCond,1}.time>blTime(1) & CBPAstructZero{indCond,1}.time<blTime(2)),4)...
        ,1,1,1,size(CBPAstructZero{indCond,1}.powspctrm,4)));
end

CBPAstructMerged.powspctrmdimord = 'rpt_chan_freq_time';
CBPAstructMerged.time = time;
CBPAstructMerged.freq = freq(1:end-1);
CBPAstructMerged.label = elec.label;
CBPAstructMerged.powspctrm = squeeze(nanmean(permute(NormalizedSSVEP(1:47,1:4,:,:,:),[1,2,3,5,4]),2));
CBPAstructMergedZero = CBPAstructMerged;
CBPAstructMergedZero.powspctrm = repmat(nanmean(...
    CBPAstructMergedZero.powspctrm(:,:,:,...
    CBPAstructMergedZero.time>blTime(1) & CBPAstructMergedZero.time<blTime(2)),4)...
    ,1,1,1,numel(CBPAstructMergedZero.time));

%figure; imagesc(squeeze(nanmean(nanmean(CBPAstructMerged.powspctrm-CBPAstructMergedZero.powspctrm,2),1)))

% prepare_neighbours determines what sensors may form clusters
cfg_neighb.method       = 'template';
cfg_neighb.template     = 'elec1010_neighb.mat';
cfg_neighb.channel      = elec.label;

cfgStat = [];
cfgStat.channel          = 'all';
cfgStat.method           = 'montecarlo';
cfgStat.frequency        = 30;
cfgStat.latency          = [3 6];
cfgStat.avgovertime      = 'yes';
cfgStat.statistic        = 'ft_statfun_depsamplesregrT';
cfgStat.correctm         = 'cluster';
cfgStat.clusteralpha     = 0.05;
cfgStat.clusterstatistic = 'maxsum';
cfgStat.minnbchan        = 2;
cfgStat.tail             = 0;
cfgStat.clustertail      = 0;
cfgStat.alpha            = 0.025;
cfgStat.numrandomization = 500;
cfgStat.parameter        = 'powspctrm';
cfgStat.neighbours       = ft_prepare_neighbours(cfg_neighb, CBPAstructMergedZero);

subj = size(CBPAstructMergedZero.powspctrm,1);
conds = 2;
design = zeros(2,conds*subj);
for indCond = 1:conds
for i = 1:subj
    design(1,(indCond-1)*subj+i) = indCond;
    design(2,(indCond-1)*subj+i) = i;
end
end
cfgStat.design   = design;
cfgStat.ivar     = 1;
cfgStat.uvar     = 2;

[stat_Norm_vsBL] = ft_freqstatistics(cfgStat, CBPAstructMergedZero, CBPAstructMerged);

%% CBPA: parametric load effect: Normalized

cfgStat = [];
cfgStat.channel          = 'all';
cfgStat.frequency        = 30;
cfgStat.latency          = [3 6];
cfgStat.avgovertime      = 'yes';
cfgStat.method           = 'montecarlo';
cfgStat.statistic        = 'ft_statfun_depsamplesregrT';
cfgStat.correctm         = 'cluster';
cfgStat.clusteralpha     = 0.05;
cfgStat.clusterstatistic = 'maxsum';
cfgStat.minnbchan        = 2;
cfgStat.tail             = 0;
cfgStat.clustertail      = 0;
cfgStat.alpha            = 0.025;
cfgStat.numrandomization = 500;
cfgStat.parameter        = 'powspctrm';
cfgStat.neighbours       = ft_prepare_neighbours(cfg_neighb, CBPAstruct{1});

subj = size(CBPAstruct{1}.powspctrm,1);
conds = 4;
design = zeros(2,conds*subj);
for indCond = 1:conds
for i = 1:subj
    design(1,(indCond-1)*subj+i) = indCond;
    design(2,(indCond-1)*subj+i) = i;
end
end
cfgStat.design   = design;
cfgStat.ivar     = 1;
cfgStat.uvar     = 2;

[stat_Norm_param] = ft_freqstatistics(cfgStat, CBPAstruct{1}, CBPAstruct{2}, CBPAstruct{3},CBPAstruct{4});

figure; imagesc(squeeze(stat_Norm_param.stat))

%save('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S2B_TFR_v4/B_data/H_YA_vsBaseline_3D.mat', 'stat', 'cfgStat');

%% original vs. baseline

CBPAstruct = []; CBPAstructZero = [];
for indCond = 1:4
    CBPAstruct{indCond,1}.powspctrmdimord = 'rpt_chan_freq_time';
    CBPAstruct{indCond,1}.time = time;
    CBPAstruct{indCond,1}.freq = freq;
    CBPAstruct{indCond,1}.label = elec.label;
    CBPAstruct{indCond,1}.powspctrm = squeeze(permute(OriginalSSVEP_bl(1:47,indCond,:,:,:),[1,2,3,5,4]));
    CBPAstructZero{indCond,1} = CBPAstruct{indCond,1};
    CBPAstructZero{indCond,1}.powspctrm = squeeze(repmat(nanmean(...
        CBPAstructZero{indCond,1}.powspctrm(:,:,:,...
        CBPAstructZero{indCond,1}.time>blTime(1) & CBPAstructZero{indCond,1}.time<blTime(2)),4)...
        ,1,1,1,size(CBPAstructZero{indCond,1}.powspctrm,4)));
end

CBPAstructMerged.powspctrmdimord = 'rpt_chan_freq_time';
CBPAstructMerged.time = time;
CBPAstructMerged.freq = freq;
CBPAstructMerged.label = elec.label;
CBPAstructMerged.powspctrm = squeeze(nanmean(permute(OriginalSSVEP_bl(1:47,1:4,:,:,:),[1,2,3,5,4]),2));
CBPAstructMergedZero = CBPAstructMerged;
CBPAstructMergedZero.powspctrm = repmat(nanmean(...
    CBPAstructMergedZero.powspctrm(:,:,:,...
    CBPAstructMergedZero.time>blTime(1) & CBPAstructMergedZero.time<blTime(2)),4)...
    ,1,1,1,numel(CBPAstructMergedZero.time));

cfgStat = [];
cfgStat.channel          = 'all';
cfgStat.method           = 'montecarlo';
cfgStat.frequency        = 30;
cfgStat.latency          = [3 6];
cfgStat.avgovertime      = 'yes';
cfgStat.statistic        = 'ft_statfun_depsamplesregrT';
cfgStat.correctm         = 'cluster';
cfgStat.clusteralpha     = 0.05;
cfgStat.clusterstatistic = 'maxsum';
cfgStat.minnbchan        = 2;
cfgStat.tail             = 0;
cfgStat.clustertail      = 0;
cfgStat.alpha            = 0.025;
cfgStat.numrandomization = 500;
cfgStat.parameter        = 'powspctrm';
cfgStat.neighbours       = ft_prepare_neighbours(cfg_neighb, CBPAstructMergedZero);

subj = size(CBPAstructMergedZero.powspctrm,1);
conds = 2;
design = zeros(2,conds*subj);
for indCond = 1:conds
for i = 1:subj
    design(1,(indCond-1)*subj+i) = indCond;
    design(2,(indCond-1)*subj+i) = i;
end
end
cfgStat.design   = design;
cfgStat.ivar     = 1;
cfgStat.uvar     = 2;

[stat_Orig_vsBL] = ft_freqstatistics(cfgStat, CBPAstructMergedZero, CBPAstructMerged);

%% original parametric load effect

cfgStat = [];
cfgStat.channel          = 'all';
cfgStat.frequency        = 30;
cfgStat.latency          = [3 6];
cfgStat.avgovertime      = 'yes';
cfgStat.method           = 'montecarlo';
cfgStat.statistic        = 'ft_statfun_depsamplesregrT';
cfgStat.correctm         = 'cluster';
cfgStat.clusteralpha     = 0.05;
cfgStat.clusterstatistic = 'maxsum';
cfgStat.minnbchan        = 2;
cfgStat.tail             = 0;
cfgStat.clustertail      = 0;
cfgStat.alpha            = 0.025;
cfgStat.numrandomization = 500;
cfgStat.parameter        = 'powspctrm';
cfgStat.neighbours       = ft_prepare_neighbours(cfg_neighb, CBPAstruct{1});

subj = size(CBPAstruct{1}.powspctrm,1);
conds = 4;
design = zeros(2,conds*subj);
for indCond = 1:conds
for i = 1:subj
    design(1,(indCond-1)*subj+i) = indCond;
    design(2,(indCond-1)*subj+i) = i;
end
end
cfgStat.design   = design;
cfgStat.ivar     = 1;
cfgStat.uvar     = 2;

[stat_Orig_param] = ft_freqstatistics(cfgStat, CBPAstruct{1}, CBPAstruct{2}, CBPAstruct{3},CBPAstruct{4});

%% save CBPA results

save(fullfile(rootpath, 'data', ['c_CBPA_',group,'.mat']),...
    'stat_Norm_vsBL', 'stat_Norm_param','stat_Orig_vsBL', 'stat_Orig_param')

%% plot CBPA effects

cBrew = brewermap(500,'RdBu');
cBrew = flipud(cBrew);

cfg = [];
cfg.layout = 'acticap-64ch-standard2.mat';
cfg.parameter = 'powspctrm';
cfg.comment = 'no';
cfg.colorbar = 'EastOutside';

h = figure('units','normalized','position',[.1 .1 .7 .7]);
subplot(2,2,1)
    cfg.zlim = [-5 5];
    cfg.highlight = 'on';
    cfg.highlightchannel = stat_Norm_vsBL.label(stat_Norm_vsBL.mask>0);
    cfg.figure = h;
    plotData = [];
    plotData.label = stat_Norm_vsBL.label; % {1 x N}
    plotData.dimord = 'chan';
    plotData.powspctrm = squeeze(nanmean(stat_Norm_vsBL.stat(:,1),2));
    ft_topoplotER(cfg,plotData);
    pval = []; pval = convertPtoExponential(stat_Norm_vsBL.posclusters(1).prob);
    cb = colorbar(cfg.colorbar); set(get(cb,'ylabel'),'string','t-values');
    title({'Spectrally normalized SSVEP (30 Hz)';['Stimulus vs. baseline: p = ', pval{1}]})
subplot(2,2,2)
    cfg.zlim = [-5 5];
    cfg.highlight = 'on';
    cfg.highlightchannel = stat_Norm_param.label(stat_Norm_param.mask>0);
    cfg.figure = h;
    plotData = [];
    plotData.label = stat_Norm_param.label; % {1 x N}
    plotData.dimord = 'chan';
    plotData.powspctrm = squeeze(nanmean(stat_Norm_param.stat(:,1),2));
    ft_topoplotER(cfg,plotData);
    cb = colorbar(cfg.colorbar); set(get(cb,'ylabel'),'string','t-values');
    title({'Spectrally normalized SSVEP (30 Hz)'})
subplot(2,2,3)
    cfg.zlim = [-5 5];
    cfg.highlight = 'on';
    cfg.highlightchannel = stat_Orig_vsBL.label(nanmean(stat_Orig_vsBL.mask(:,1),2)>0);
    cfg.figure = h;
    plotData = [];
    plotData.label = stat_Orig_vsBL.label; % {1 x N}
    plotData.dimord = 'chan';
    plotData.powspctrm = squeeze(nanmean(stat_Orig_vsBL.stat(:,1),2));
    ft_topoplotER(cfg,plotData);
%     pval = []; pval = convertPtoExponential(stat_Orig_vsBL.negclusters(1).prob);
%     cb = colorbar(cfg.colorbar); set(get(cb,'ylabel'),'string','t-values');
%     title({'Original SSVEP (30 Hz)';['Stimulus vs. baseline: p = ', pval{1}]})
subplot(2,2,4)
    cfg.zlim = [-5 5];
    cfg.highlight = 'on';
    cfg.highlightchannel = stat_Orig_param.label(nanmean(stat_Orig_param.mask(:,1),2)>0);
    cfg.figure = h;
    plotData = [];
    plotData.label = stat_Orig_param.label; % {1 x N}
    plotData.dimord = 'chan';
    plotData.powspctrm = squeeze(nanmean(stat_Orig_param.stat(:,1),2));
    ft_topoplotER(cfg,plotData);
%     pval = []; pval = convertPtoExponential(stat_Orig_param.negclusters(1).prob);
%     cb = colorbar(cfg.colorbar); set(get(cb,'ylabel'),'string','t-values');
%     title({'Original SSVEP (30 Hz)';['parametric effect: p = ', pval{1}]})

set(findall(gcf,'-property','FontSize'),'FontSize',30)
colormap(cBrew)

figureName = ['c_CBPA_topographies_', group];
saveas(h, fullfile(pn.figures, figureName), 'epsc');
saveas(h, fullfile(pn.figures, figureName), 'png');
