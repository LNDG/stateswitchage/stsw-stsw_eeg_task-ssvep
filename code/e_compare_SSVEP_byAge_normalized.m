% plot SSVEP overview plot

if ismac % run if function is not pre-compiled
    currentFile = mfilename('fullpath');
    [pathstr,~,~] = fileparts(currentFile);
    cd(fullfile(pathstr,'..'))
    rootpath = pwd;
end

pn.dataIn	= fullfile(rootpath, '..', 'X1_preprocEEGData');
pn.out      = fullfile(rootpath, 'data', 'individualFFT');
pn.tools	= fullfile(rootpath, 'tools'); addpath(pn.tools);
pn.figures  = fullfile(rootpath, 'figures');
addpath(fullfile(pn.tools, 'fieldtrip')); ft_defaults;
addpath(fullfile(pn.tools, 'BrewerMap'));
addpath(fullfile(pn.tools, 'shadedErrorBar'));
addpath(fullfile(pn.tools, 'RainCloudPlots'));

pn.summary	= fullfile(rootpath, '..', '..', 'stsw_multimodal', 'data');

cBrew = brewermap(500,'RdBu');
cBrew = flipud(cBrew);

filename = fullfile(rootpath, 'code', 'id_list.txt');
fileID = fopen(filename);
IDs = textscan(fileID,'%s');
fclose(fileID);
IDs = IDs{1};

%% extract SSVEP traces

NormalizedSSVEP = []; OriginalSSVEP = []; NormalizedSSVEP = []; SSVEPMerged = [];
for indID = 1:numel(IDs)
    tmp = load(fullfile(pn.out, [IDs{indID},'_SSVEP.mat']));
    for indCond = 1:4
        OriginalSSVEP(indID,indCond,:,:,:) = tmp.MTMdata{indCond}.powspctrm_avg;
    end
    SSVEPtime = tmp.MTMdata{indCond}.time;
    SSVEPfreq = tmp.MTMdata{indCond}.freq;
end
for indFreq = 2:30-1
   NormalizedSSVEP(:,:,:,:,indFreq) = (OriginalSSVEP(:,:,:,:,indFreq)-nanmean(OriginalSSVEP(:,:,:,:,[indFreq-1,indFreq+1]),5))./nanmean(OriginalSSVEP(:,:,:,:,[indFreq-1,indFreq+1]),5);
end
% baseline correction
blTimeIdx = find(tmp.MTMdata{indCond}.time > 2.3 & tmp.MTMdata{indCond}.time <2.9);
originalBaseline = repmat(nanmean(nanmean(OriginalSSVEP(:,:,:,blTimeIdx,:),4),2),1,4,1,66,1);
normalizedBaseline = repmat(nanmean(nanmean(NormalizedSSVEP(:,:,:,blTimeIdx,:),4),2),1,4,1,66,1);
OriginalSSVEP_bl = (OriginalSSVEP-originalBaseline)./originalBaseline;
OriginalSSVEP_bl = squeeze(OriginalSSVEP_bl(:,:,:,:, find(SSVEPfreq>=30, 1, 'first')));
NormalizedSSVEP_bl = (NormalizedSSVEP-normalizedBaseline);
NormalizedSSVEP_bl = squeeze(NormalizedSSVEP_bl(:,:,:,:, find(SSVEPfreq>=30, 1, 'first')));

idx_YA = 1:47;
idx_OA = 48:numel(IDs);

%% create Figure

h = figure('units','normalized','position',[.1 .1 .3 .2]);
set(gcf,'renderer','Painters')

    %% plot SSVEP traces for YA
    
    idxChanSSVEP = 58:60;
    SSVEPMerged = squeeze(nanmean(NormalizedSSVEP_bl(idx_YA,:,idxChanSSVEP,:),3));
    
    subplot(1,2,1)
    cla; hold on;
    
    % add background shading to indicate stimulus period
    
    % highlight different experimental phases in background
    patches.timeVec = [0 3.066];
    patches.colorVec = [1 .95 .8];
    for indP = 1:size(patches.timeVec,2)-1
        YLim = [-.1 .5];
        p = patch([patches.timeVec(indP) patches.timeVec(indP+1) patches.timeVec(indP+1) patches.timeVec(indP)], ...
                    [YLim(1) YLim(1)  YLim(2), YLim(2)], patches.colorVec(indP,:));
        p.EdgeColor = 'none';
    end
    
    % new value = old value ? subject average + grand average
    time = SSVEPtime-3;
    condAvg = squeeze(nanmean(SSVEPMerged,2));
    curData = squeeze(SSVEPMerged(:,1,:));
    curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l1 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', 8.*[.1 .1 .1],'linewidth', 2}, 'patchSaturation', .1);
    curData = squeeze(SSVEPMerged(:,2,:));
    curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l2 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', 6.*[.1 .1 .1],'linewidth', 2}, 'patchSaturation', .1);
    curData = squeeze(SSVEPMerged(:,3,:));
    curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l3 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', 4.*[.1 .1 .1],'linewidth', 2}, 'patchSaturation', .1);
    curData = squeeze(SSVEPMerged(:,4,:));
    curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l4 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', 2.*[.1 .1 .1],'linewidth', 2}, 'patchSaturation', .1);
    xlabel('Time (s from stimulus onset); response-locked')
%     line([3 3]-3,get(gca, 'Ylim'), get(gca, 'Ylim'), 'Color', 'k', 'LineWidth', 2, 'LineStyle', '--')
%     line([6 6]-3,get(gca, 'Ylim'), get(gca, 'Ylim'), 'Color', 'k', 'LineWidth', 2, 'LineStyle', '--')
    legend([l1.mainLine, l2.mainLine, l3.mainLine, l4.mainLine], {'Load 1'; 'Load 2'; 'Load 3'; 'Load 4'}, 'orientation', 'vertical', 'location', 'South'); legend('boxoff');
    xlim([-1 4]); ylim([-.1 .5])
    ylabel({'relative SSVEP';'Power (a.u.)'});
    xlabel({'Time (s from stimulus onset)'});
    title({'SpecNorm SSVEP (YA)'})
    set(findall(gcf,'-property','FontSize'),'FontSize',17)
   
   %% plot SSVEP traces for OA
    
    idxChanSSVEP = 58:60;
    SSVEPMerged = squeeze(nanmean(NormalizedSSVEP_bl(idx_OA,:,idxChanSSVEP,:),3));
    
    subplot(1,2,2)
    cla; hold on;
    
    % add background shading to indicate stimulus period
    
    % highlight different experimental phases in background
    patches.timeVec = [0 3.066];
    patches.colorVec = [1 .95 .8];
    for indP = 1:size(patches.timeVec,2)-1
        YLim = [-.1 .5];
        p = patch([patches.timeVec(indP) patches.timeVec(indP+1) patches.timeVec(indP+1) patches.timeVec(indP)], ...
                    [YLim(1) YLim(1)  YLim(2), YLim(2)], patches.colorVec(indP,:));
        p.EdgeColor = 'none';
    end
    
    % new value = old value ? subject average + grand average
    time = SSVEPtime-3;
    condAvg = squeeze(nanmean(SSVEPMerged,2));
    curData = squeeze(SSVEPMerged(:,1,:));
    curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l1 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', 8.*[.1 .1 .1],'linewidth', 2}, 'patchSaturation', .1);
    curData = squeeze(SSVEPMerged(:,2,:));
    curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l2 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', 6.*[.1 .1 .1],'linewidth', 2}, 'patchSaturation', .1);
    curData = squeeze(SSVEPMerged(:,3,:));
    curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l3 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', 4.*[.1 .1 .1],'linewidth', 2}, 'patchSaturation', .1);
    curData = squeeze(SSVEPMerged(:,4,:));
    curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l4 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', 2.*[.1 .1 .1],'linewidth', 2}, 'patchSaturation', .1);
    xlabel('Time (s from stimulus onset); response-locked')
%     line([3 3]-3,get(gca, 'Ylim'), get(gca, 'Ylim'), 'Color', 'k', 'LineWidth', 2, 'LineStyle', '--')
%     line([6 6]-3,get(gca, 'Ylim'), get(gca, 'Ylim'), 'Color', 'k', 'LineWidth', 2, 'LineStyle', '--')
    legend([l1.mainLine, l2.mainLine, l3.mainLine, l4.mainLine], {'Load 1'; 'Load 2'; 'Load 3'; 'Load 4'}, 'orientation', 'vertical', 'location', 'South'); legend('boxoff');
    xlim([-1 4]); ylim([-.1 .5])
    ylabel({'relative SSVEP';'Power (a.u.)'});
    xlabel({'Time (s from stimulus onset)'});
    title({'SpecNorm SSVEP (OA)'})
    set(findall(gcf,'-property','FontSize'),'FontSize',17)
   
   figureName = 'e_SSVEP_byAge';
   saveas(h, fullfile(pn.figures, figureName), 'epsc');
   saveas(h, fullfile(pn.figures, figureName), 'png');
   
   %% median split on drift rates L1
   
    idxChanSSVEP = 58:60;
    SSVEPMerged = squeeze(nanmean(NormalizedSSVEP_bl(:,:,idxChanSSVEP,:),3));

    load(fullfile(pn.summary, 'STSWD_summary_YAOA.mat'))

    h = figure('units','normalized','position',[.1 .1 .6 .3]);
    ageIndex{1} = 1:47; ageIndex{2} = 48:numel(IDs);
    for indAge = 1:2
        % perform a median split
        idx_summary = ismember(STSWD_summary.IDs,IDs(ageIndex{indAge}));
        [sortVal, sortIdx] = sort(STSWD_summary.HDDM_vat.driftEEG(idx_summary,1),'ascend');
        sortIdx = ageIndex{indAge}(sortIdx);
        idx_lowdrift = sortIdx(1:ceil(numel(sortIdx)/2));
        idx_highdrift = sortIdx(ceil(numel(sortIdx)/2)+1:end);
        subplot(1,2,indAge); cla; hold on;

        curData = squeeze(nanmean(SSVEPMerged(idx_lowdrift,1,:),2));
        standError = nanstd(curData,1)./sqrt(size(curData,1));
        low = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', 'black','linewidth', 2}, 'patchSaturation', .1);
        shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', 'black','linewidth', 2}, 'patchSaturation', .1);

        curData = squeeze(nanmean(SSVEPMerged(idx_highdrift,1,:),2));
        standError = nanstd(curData,1)./sqrt(size(curData,1));
        high = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', 'red','linewidth', 2}, 'patchSaturation', .1);
        shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', 'red','linewidth', 2}, 'patchSaturation', .1);

        legend([low.mainLine, high.mainLine], {'low drift', 'high drift'}, 'location', 'SouthEast'); legend('boxoff')

        xlabel('Time (ms); response-locked')
        xlim([-2 5]); %ylim([-5.5, -4])
        ylabel({'SSVEP'});
        xlabel({'Time (s from stimulus onset)'});
        set(findall(gcf,'-property','FontSize'),'FontSize',13)
    end

   
    idxChanSSVEP = 58:60;
    SSVEPMerged = squeeze(nanmean(OriginalSSVEP_bl(:,:,idxChanSSVEP,:),3));

    load(fullfile(pn.summary, 'STSWD_summary_YAOA.mat'))

    h = figure('units','normalized','position',[.1 .1 .6 .3]);
    ageIndex{1} = 1:47; ageIndex{2} = 48:numel(IDs);
    for indAge = 1:2
        % perform a median split
        idx_summary = ismember(STSWD_summary.IDs,IDs(ageIndex{indAge}));
        [sortVal, sortIdx] = sort(STSWD_summary.HDDM_vat.driftEEG(idx_summary,1),'ascend');
        sortIdx = ageIndex{indAge}(sortIdx);
        idx_lowdrift = sortIdx(1:ceil(numel(sortIdx)/2));
        idx_highdrift = sortIdx(ceil(numel(sortIdx)/2)+1:end);
        subplot(1,2,indAge); cla; hold on;

        curData = squeeze(nanmean(SSVEPMerged(idx_lowdrift,1,:),2));
        standError = nanstd(curData,1)./sqrt(size(curData,1));
        low = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', 'black','linewidth', 2}, 'patchSaturation', .1);
        shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', 'black','linewidth', 2}, 'patchSaturation', .1);

        curData = squeeze(nanmean(SSVEPMerged(idx_highdrift,1,:),2));
        standError = nanstd(curData,1)./sqrt(size(curData,1));
        high = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', 'red','linewidth', 2}, 'patchSaturation', .1);
        shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', 'red','linewidth', 2}, 'patchSaturation', .1);

        legend([low.mainLine, high.mainLine], {'low drift', 'high drift'}, 'location', 'SouthEast'); legend('boxoff')

        xlabel('Time (ms); response-locked')
        xlim([-2 5]); %ylim([-5.5, -4])
        ylabel({'SSVEP'});
        xlabel({'Time (s from stimulus onset)'});
        set(findall(gcf,'-property','FontSize'),'FontSize',13)
    end