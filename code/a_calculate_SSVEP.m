function a_calculate_SSVEP(id, rootpath)

%% compute SSVEPs

% Here, we extract the 30 Hz SSVEP using intervals covering 15 cycles with
% a step size of 250 ms. Then we offer multiple outputs regarding
% normalization: spectral correction means that the average of the two
% adjacent frequency bands is subtracted from the 30 Hz estimate,
% effectively controlling for inter-individual differences in spectral
% slope and more broad-band noise around 30 Hz band.

% 190125 | JQK created script

% v5: CSD-transform, saves single-trial log10-transformed data

if ismac % run if function is not pre-compiled
    id = '1126'; % test for example subject
    currentFile = mfilename('fullpath');
    [pathstr,~,~] = fileparts(currentFile);
    cd(fullfile(pathstr,'..'))
    rootpath = pwd;
end

pn.dataIn	= fullfile(rootpath, '..', 'X1_preprocEEGData');
pn.data     = fullfile(rootpath, 'data');
pn.out      = fullfile(rootpath, 'data', 'individualFFT');
    if ~exist(pn.out); mkdir(pn.out); end
pn.tools	= fullfile(rootpath, 'tools'); addpath(pn.tools);
addpath(fullfile(pn.tools, 'fieldtrip')); ft_defaults;

display(['processing ID ', id]);

load(fullfile(pn.dataIn, [id, '_dynamic_EEG_Rlm_Fhl_rdSeg_Art_Cond.mat']), 'data');
load(fullfile(pn.data, 'elec.mat'))

 %% CSD transform

TrlInfo = data.TrlInfo;
TrlInfoLabels = data.TrlInfoLabels;

csd_cfg = [];
csd_cfg.elecfile = 'standard_1005.elc';
csd_cfg.method = 'spline';
data = ft_scalpcurrentdensity(csd_cfg, data);
data.TrlInfo = TrlInfo; clear TrlInfo;
data.TrlInfoLabels = TrlInfoLabels; clear TrlInfoLabels;

%% calculate FFT for stim period from ERP

ncyc = 30;
timeWindow = ncyc.*(1/30); % 1s sampling window
stepSize = .1; % 100 ms step size
timeWindows = 1:stepSize:8-(timeWindow./2);

MTMdata = cell(1,4);

for indCond = 1:4
    for indWindow = 1:numel(timeWindows)
        cfg = [];
        cfg.trials = find(data.TrlInfo(:,8)==indCond);
        cfg.toilim = [timeWindows(indWindow) timeWindows(indWindow)+timeWindow];
        curData = ft_redefinetrial(cfg, data);

        curData = rmfield(curData, 'TrlInfo');
        curData = rmfield(curData, 'TrlInfoLabels');

        cfg              = [];
        cfg.output       = 'pow';
        cfg.channel      = 'all';
        cfg.method       = 'mtmfft';
        cfg.taper        = 'hanning';
        cfg.keeptrials   = 'yes';
        %cfg.tapsmofrq    = 0;
        cfg.pad          = ceil(size(curData.time{1},2)/30)*30/100; % padding in s, such that cfg.foi/T is integer
        cfg.foi          = [2:2:60];
        %cfg.foi          = [ncyc-1:1:ncyc+1]/timeWindow;

        tmpData = ft_freqanalysis(cfg, curData);

        %figure; imagesc(squeeze(nanmean(tmpData.powspctrm,1)))

        MTMdata{indCond}.powspctrm(:,:,indWindow,:) = tmpData.powspctrm;
        MTMdata{indCond}.dimord = 'rpt_chan_time_freq';
        MTMdata{indCond}.time(indWindow) = mean([timeWindows(indWindow) timeWindows(indWindow)+timeWindow]);
    end
    MTMdata{indCond}.label = tmpData.label;
    MTMdata{indCond}.freq = tmpData.freq;
    MTMdata{indCond}.elec = elec;
    MTMdata{indCond}.cfg = tmpData.cfg;
    clear tmpData;
end

% averaging and spectral correction using adjacent frequency bands

for indCond = 1:4
    MTMdata{indCond}.powspctrm_avg = squeeze(nanmean(MTMdata{indCond}.powspctrm,1));
    MTMdata{indCond}.powspctrm_avg_log10 = squeeze(nanmean(log10(MTMdata{indCond}.powspctrm),1));
    MTMdata{indCond}.powspctrm_avg_SpectNorm13 = squeeze(nanmean(MTMdata{indCond}.powspctrm(:,:,:,15)-...
        nanmean(MTMdata{indCond}.powspctrm(:,:,:,[14 16]),4),1));
    MTMdata{indCond}.dimord = 'chan_time_freq';
    MTMdata{indCond} = rmfield(MTMdata{indCond}, 'powspctrm');
end

%% save output

save(fullfile(pn.out, [id, '_SSVEP.mat']), 'MTMdata');

end