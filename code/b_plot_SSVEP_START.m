clc; close all; clear all;

currentFile = mfilename('fullpath');
[pathstr,~,~] = fileparts(currentFile);
cd(fullfile(pathstr,'..'))
pn.root = pwd;

addpath(fullfile(pn.root, 'code'));

b_plot_SSVEP('YA')
b_plot_SSVEP('OA')
b_plot_SSVEP('YAOA')

disp('Step finished!')

close all; clc;