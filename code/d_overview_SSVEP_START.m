clc; close all; clear all;

currentFile = mfilename('fullpath');
[pathstr,~,~] = fileparts(currentFile);
cd(fullfile(pathstr,'..'))
pn.root = pwd;

addpath(fullfile(pn.root, 'code'));

d_overview_SSVEP('YA')
d_overview_SSVEP('OA')
d_overview_SSVEP('YAOA')

disp('Step finished!')

close all; clc;