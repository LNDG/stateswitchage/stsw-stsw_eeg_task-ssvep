function d_overview_SSVEP(group)

if ismac % run if function is not pre-compiled
    currentFile = mfilename('fullpath');
    [pathstr,~,~] = fileparts(currentFile);
    cd(fullfile(pathstr,'..'))
    rootpath = pwd;
end

if strcmp(group, 'YA')
    IDs = {'1117';'1118';'1120';'1124';'1126';'1131';'1132';'1135';'1136';'1138';...
        '1144';'1151';'1158';'1160';'1163';'1164';'1167';'1169';'1172';'1173';...
        '1178';'1182';'1215';'1216';'1219';'1221';'1223';'1227';'1228';'1233';...
        '1234';'1237';'1239';'1240';'1243';'1245';'1247';'1250';'1252';'1257';...
        '1261';'1265';'1266';'1268';'1270';'1276';'1281'};
elseif strcmp(group, 'OA')
    IDs = {'2104';'2107';'2108';'2112';'2118';'2120';'2121';'2123';'2125';'2129';...
        '2130';'2131';'2132';'2133';'2134';'2135';'2139';'2140';'2145';'2147';...
        '2149';'2157';'2160';'2201';'2202';'2203';'2205';'2206';'2209';'2210';...
        '2211';'2213';'2214';'2215';'2216';'2217';'2219';'2222';'2224';'2226';...
        '2227';'2236';'2237';'2238';'2241';'2244';'2246';'2248';'2250';'2251';...
        '2252';'2258';'2261'};
elseif strcmp(group, 'YAOA')
    filename = fullfile(rootpath, 'code', 'id_list.txt');
    fileID = fopen(filename);
    IDs = textscan(fileID,'%s');
    fclose(fileID);
    IDs = IDs{1};
end

pn.dataIn	= fullfile(rootpath, '..', 'X1_preprocEEGData');
pn.out      = fullfile(rootpath, 'data', 'individualFFT');
pn.tools	= fullfile(rootpath, 'tools'); addpath(pn.tools);
pn.figures  = fullfile(rootpath, 'figures');
addpath(fullfile(pn.tools, 'fieldtrip')); ft_defaults;
addpath(fullfile(pn.tools, 'BrewerMap'));
addpath(fullfile(pn.tools, 'shadedErrorBar'));
addpath(genpath(fullfile(pn.tools, 'RainCloudPlots')));

cBrew = brewermap(500,'RdBu');
cBrew = flipud(cBrew);

filename = fullfile(rootpath, 'code', 'id_list.txt');
fileID = fopen(filename);
IDs = textscan(fileID,'%s');
fclose(fileID);
IDs = IDs{1};

%% extract SSVEP traces

NormalizedSSVEP = []; OriginalSSVEP = []; NormalizedSSVEP = []; SSVEPMerged = [];
for indID = 1:numel(IDs)
    tmp = load(fullfile(pn.out, [IDs{indID},'_SSVEP.mat']));
    for indCond = 1:4
        OriginalSSVEP(indID,indCond,:,:,:) = tmp.MTMdata{indCond}.powspctrm_avg;
    end
    SSVEPtime = tmp.MTMdata{indCond}.time;
    SSVEPfreq = tmp.MTMdata{indCond}.freq;
end
for indFreq = 2:30-1
   NormalizedSSVEP(:,:,:,:,indFreq) = (OriginalSSVEP(:,:,:,:,indFreq)-nanmean(OriginalSSVEP(:,:,:,:,[indFreq-1,indFreq+1]),5))./nanmean(OriginalSSVEP(:,:,:,:,[indFreq-1,indFreq+1]),5);
end
% baseline correction
blTimeIdx = find(tmp.MTMdata{indCond}.time > 2.3 & tmp.MTMdata{indCond}.time <2.9);
originalBaseline = repmat(nanmean(nanmean(OriginalSSVEP(:,:,:,blTimeIdx,:),4),2),1,4,1,66,1);
normalizedBaseline = repmat(nanmean(nanmean(NormalizedSSVEP(:,:,:,blTimeIdx,:),4),2),1,4,1,66,1);
OriginalSSVEP_bl = (OriginalSSVEP-originalBaseline)./originalBaseline;
OriginalSSVEP_bl = squeeze(OriginalSSVEP_bl(:,:,:,:, find(SSVEPfreq>=30, 1, 'first')));
NormalizedSSVEP_bl = (NormalizedSSVEP-normalizedBaseline);
NormalizedSSVEP_bl = squeeze(NormalizedSSVEP_bl(:,:,:,:, find(SSVEPfreq>=30, 1, 'first')));

load(fullfile(rootpath, 'data',['c_CBPA_',group,'.mat']),...
    'stat_Norm_vsBL', 'stat_Norm_param','stat_Orig_vsBL', 'stat_Orig_param')

%% add single-trial SSVEP topographies (bl & change) (spectrally corrected; need to perform permutations)

h = figure('units','normalized','position',[.1 .1 .3 .4]);
set(gcf,'renderer','Painters')

cfg = [];
cfg.layout = 'acticap-64ch-standard2.mat';
cfg.parameter = 'powspctrm';
cfg.comment = 'no';
cfg.colorbar = 'EastOutside';
cfg.colormap = cBrew;

subplot(2,2,1)
    cfg.marker = 'off';  
    cfg.highlight = 'yes';
    cfg.highlightchannel = stat_Norm_vsBL.label(stat_Norm_vsBL.posclusterslabelmat>0);
    cfg.highlightcolor = [0 0 0];
    cfg.highlightsymbol = '.';
    cfg.highlightsize = 18;
    cfg.zlim = [-5 5];
    cfg.figure = h;
    plotData = [];
    plotData.label = stat_Norm_vsBL.label; % {1 x N}
    plotData.dimord = 'chan';
    plotData.powspctrm = squeeze(nanmean(stat_Norm_vsBL.stat(:,1),2));
    ft_topoplotER(cfg,plotData);
    pval = []; pval = convertPtoExponential(stat_Norm_vsBL.posclusters(1).prob);
    cb = colorbar(cfg.colorbar); set(get(cb,'ylabel'),'string','t-values');
    title({['Stimulus vs. baseline: p = ', pval{1}]})
subplot(2,2,2)
    cfg.marker = 'off';  
    cfg.highlight = 'yes';
    cfg.highlightchannel = stat_Norm_param.label(stat_Norm_param.mask>0);
    cfg.highlightcolor = [0 0 0];
    cfg.highlightsymbol = '.';
    cfg.highlightsize = 18;
    cfg.zlim = [-5 5];
    cfg.figure = h;
    plotData = [];
    plotData.label = stat_Norm_param.label; % {1 x N}
    plotData.dimord = 'chan';
    plotData.powspctrm = squeeze(nanmean(stat_Norm_param.stat(:,1),2));
    ft_topoplotER(cfg,plotData);
    cb = colorbar(cfg.colorbar); set(get(cb,'ylabel'),'string','t-values');
    title({'Spectrally normalized SSVEP (30 Hz)';'Parametric effect: no cluster'})

    %% plot SSVEP traces
    
    idxChanSSVEP = 58:60;
    SSVEPMerged = squeeze(nanmean(NormalizedSSVEP_bl(:,:,idxChanSSVEP,:),3));
    
    subplot(2,2,3)
    cla; hold on;
    
    % add background shading to indicate stimulus period
    
    % highlight different experimental phases in background
    patches.timeVec = [0 3.066];
    patches.colorVec = [1 .95 .8];
    for indP = 1:size(patches.timeVec,2)-1
        YLim = [-.1 .5];
        p = patch([patches.timeVec(indP) patches.timeVec(indP+1) patches.timeVec(indP+1) patches.timeVec(indP)], ...
                    [YLim(1) YLim(1)  YLim(2), YLim(2)], patches.colorVec(indP,:));
        p.EdgeColor = 'none';
    end
    
    % new value = old value ? subject average + grand average
    time = SSVEPtime-3;
    condAvg = squeeze(nanmean(SSVEPMerged,2));
    curData = squeeze(SSVEPMerged(:,1,:));
    curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l1 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', 8.*[.1 .1 .1],'linewidth', 2}, 'patchSaturation', .1);
    curData = squeeze(SSVEPMerged(:,2,:));
    curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l2 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', 6.*[.1 .1 .1],'linewidth', 2}, 'patchSaturation', .1);
    curData = squeeze(SSVEPMerged(:,3,:));
    curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l3 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', 4.*[.1 .1 .1],'linewidth', 2}, 'patchSaturation', .1);
    curData = squeeze(SSVEPMerged(:,4,:));
    curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l4 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', 2.*[.1 .1 .1],'linewidth', 2}, 'patchSaturation', .1);
    xlabel('Time (s from stimulus onset); response-locked')
%     line([3 3]-3,get(gca, 'Ylim'), get(gca, 'Ylim'), 'Color', 'k', 'LineWidth', 2, 'LineStyle', '--')
%     line([6 6]-3,get(gca, 'Ylim'), get(gca, 'Ylim'), 'Color', 'k', 'LineWidth', 2, 'LineStyle', '--')
    legend([l1.mainLine, l2.mainLine, l3.mainLine, l4.mainLine], {'Load 1'; 'Load 2'; 'Load 3'; 'Load 4'}, 'orientation', 'vertical', 'location', 'South'); legend('boxoff');
    xlim([-1 4]); ylim([-.1 .5])
    ylabel({'relative SSVEP';'Power (a.u.)'});
    xlabel({'Time (s from stimulus onset)'});
    title({'Spectrally normalized SSVEP (30 Hz)'})
    set(findall(gcf,'-property','FontSize'),'FontSize',17)

%% plot absence of load effect with raincloud visualization
    
    SSVEPMerged = squeeze(nanmean(nanmean(NormalizedSSVEP_bl(:,:,idxChanSSVEP,SSVEPtime>=3 &SSVEPtime<=6),4),3));
    
    % read into cell array of the appropriate dimensions
    data = [];
    for i = 1:4
        for j = 1:1
            data{i, j} = SSVEPMerged(:,i)';
        end
    end

    % set up figure

    cl = [.5 .5 .5];

    % make figure
    ax = subplot(2,2,4); cla;
    h_rc = rm_raincloud(data, cl,0);

    set(gca, 'YTickLabels', flip({'1'; '2'; '3'; '4'})); % label assignment also has to be flipped
    ylabel('Target load')
    xlabel({'relative SSVEP Power (a.u.)'})
    xlim([-.75 1.75]);

    %% save overview plot
    
    set(findall(gcf,'-property','FontSize'),'FontSize',18)
    
    figureName = ['d_SSVEPnormalizedOverview_',group];
    saveas(h, fullfile(pn.figures, figureName), 'epsc');
    saveas(h, fullfile(pn.figures, figureName), 'png');
    
    %% add single-trial SSVEP topographies for original data
    
    h = figure('units','normalized','position',[.1 .1 .3 .4]);
    set(gcf,'renderer','Painters')

    cfg = [];
    cfg.layout = 'acticap-64ch-standard2.mat';
    cfg.parameter = 'powspctrm';
    cfg.comment = 'no';
    cfg.colorbar = 'EastOutside';
    cfg.colormap = cBrew;

    subplot(2,2,1)
        cfg.marker = 'off';  
        cfg.highlight = 'yes';
        cfg.highlightchannel = stat_Norm_vsBL.label(stat_Orig_vsBL.negclusterslabelmat>0);
        cfg.highlightcolor = [0 0 0];
        cfg.highlightsymbol = '.';
        cfg.highlightsize = 18;
        cfg.zlim = [-5 5];
        cfg.figure = h;
        plotData = [];
        plotData.label = stat_Orig_vsBL.label; % {1 x N}
        plotData.dimord = 'chan';
        plotData.powspctrm = squeeze(nanmean(stat_Orig_vsBL.stat(:,1),2));
        ft_topoplotER(cfg,plotData);
        pval = []; pval = convertPtoExponential(stat_Orig_vsBL.negclusters(1).prob);
        cb = colorbar(cfg.colorbar); set(get(cb,'ylabel'),'string','t-values');
        title({'Standard SSVEP (30 Hz)';['Stimulus vs. baseline: p = ', pval{1}]})
    subplot(2,2,2)
        cfg.marker = 'off';  
        cfg.highlight = 'yes';
        cfg.highlightchannel = stat_Orig_param.label(stat_Orig_param.mask>0);
        cfg.highlightcolor = [0 0 0];
        cfg.highlightsymbol = '.';
        cfg.highlightsize = 18;
        cfg.zlim = [-5 5];
        cfg.figure = h;
        plotData = [];
        plotData.label = stat_Orig_param.label; % {1 x N}
        plotData.dimord = 'chan';
        plotData.powspctrm = squeeze(nanmean(stat_Orig_param.stat(:,1),2));
        ft_topoplotER(cfg,plotData);
        %pval = []; pval = convertPtoExponential(stat_Orig_param.negclusters(1).prob);
        cb = colorbar(cfg.colorbar); set(get(cb,'ylabel'),'string','t-values');
        %title({'Standard SSVEP (30 Hz)';['Parametric effect: p = ', pval{1}]})
        title({'Standard SSVEP (30 Hz)'})

    %% plot SSVEP traces
    
    idxChanSSVEP = 58:60;
    SSVEPMerged = squeeze(nanmean(OriginalSSVEP_bl(:,:,idxChanSSVEP,:),3));
    
    subplot(2,2,3)
    cla; hold on;
    
    % add background shading to indicate stimulus period
    
    % highlight different experimental phases in background
    patches.timeVec = [0 3];
    patches.colorVec = [1 .95 .8];
    for indP = 1:size(patches.timeVec,2)-1
        YLim = [-.2 .5];
        p = patch([patches.timeVec(indP) patches.timeVec(indP+1) patches.timeVec(indP+1) patches.timeVec(indP)], ...
                    [YLim(1) YLim(1)  YLim(2), YLim(2)], patches.colorVec(indP,:));
        p.EdgeColor = 'none';
    end;
    
    % new value = old value ? subject average + grand average
    time = SSVEPtime-3;
    condAvg = squeeze(nanmean(SSVEPMerged,2));
    curData = squeeze(SSVEPMerged(:,1,:));
    curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l1 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', 8.*[.1 .1 .1],'linewidth', 2}, 'patchSaturation', .1);
    curData = squeeze(SSVEPMerged(:,2,:));
    curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l2 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', 6.*[.1 .1 .1],'linewidth', 2}, 'patchSaturation', .1);
    curData = squeeze(SSVEPMerged(:,3,:));
    curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l3 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', 4.*[.1 .1 .1],'linewidth', 2}, 'patchSaturation', .1);
    curData = squeeze(SSVEPMerged(:,4,:));
    curData = curData-condAvg+repmat(nanmean(condAvg,1),size(condAvg,1),1);
    standError = nanstd(curData,1)./sqrt(size(curData,1));
    l4 = shadedErrorBar(time,nanmean(curData,1),standError, 'lineprops', {'color', 2.*[.1 .1 .1],'linewidth', 2}, 'patchSaturation', .1);
    xlabel('Time (s from stimulus onset); response-locked')
%     line([3 3]-3,get(gca, 'Ylim'), get(gca, 'Ylim'), 'Color', 'k', 'LineWidth', 2, 'LineStyle', '--')
%     line([6 6]-3,get(gca, 'Ylim'), get(gca, 'Ylim'), 'Color', 'k', 'LineWidth', 2, 'LineStyle', '--')
    legend([l1.mainLine, l2.mainLine, l3.mainLine, l4.mainLine], {'Load 1'; 'Load 2'; 'Load 3'; 'Load 4'}, 'orientation', 'vertical', 'location', 'South'); legend('boxoff');
    xlim([-1 4]); ylim([-.15 .2])
    ylabel({'relative SSVEP';'Power (a.u.)'});
    xlabel({'Time (s from stimulus onset)'});
    %title({'SSVEP power load modulation'; ''})
    set(findall(gcf,'-property','FontSize'),'FontSize',17)
   
    %% plot absence of load effect with raincloud visualization
    
    SSVEPMerged = squeeze(nanmean(nanmean(OriginalSSVEP_bl(:,:,idxChanSSVEP,SSVEPtime>=3 &SSVEPtime<=6),4),3));
    
    % read into cell array of the appropriate dimensions
    data = [];
    for i = 1:4
        for j = 1:1
            data{i, j} = SSVEPMerged(:,i)';
        end
    end

    % set up figure

    cl = [.5 .5 .5];

    % make figure
    ax = subplot(2,2,4); cla;
    h_rc = rm_raincloud(data, cl,0);

    set(gca, 'YTickLabels', flip({'1'; '2'; '3'; '4'})); % label assignment also has to be flipped
    ylabel('Target load')
    xlabel({'relative SSVEP Power (a.u.)'})
    xlim([-.75 1.75]);

    %% save overview plot
    
    set(findall(gcf,'-property','FontSize'),'FontSize',18)
    
    figureName = ['d_SSVEPoriginalOverview_', group];
    saveas(h, fullfile(pn.figures, figureName), 'epsc');
    saveas(h, fullfile(pn.figures, figureName), 'png');