clc; close all; clear all;

currentFile = mfilename('fullpath');
[pathstr,~,~] = fileparts(currentFile);
cd(fullfile(pathstr,'..'))
pn.root = pwd;

addpath(fullfile(pn.root, 'code'));

c_SSVEP_CBPA('YA')
c_SSVEP_CBPA('OA')
c_SSVEP_CBPA('YAOA')

disp('Step finished!')

close all; clc;