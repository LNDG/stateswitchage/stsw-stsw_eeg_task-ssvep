function scientificVals = convertPtoExponential(p_val)
    
% 190404 | function created by JQK

    scientificVals = cell(size(p_val));
    for indP = 1:numel(p_val)
        n = p_val(indP);
        if isnan(n)
            scientificVals{indP} = 'NaN';
        else
            n_exp=floor(log10(n));
            n_coef=n/(abs(1*10^n_exp));
            coef = sprintf('%.0f', round(n_coef,2));
            exp = sprintf('%.0f', round(n_exp,0));
            if abs(n_exp)<= 3
                scientificVals{indP} = sprintf('%.3f', round(p_val(indP),3));
            else
                scientificVals{indP} = [coef, 'e^', exp];
            end
        end
    end

end